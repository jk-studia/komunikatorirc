package pl.jarekkozmic.controllers;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class CustomLoginSceneController {
	
/* *****************************************************************************************************************
 	U�wywane Dane
*/
	@FXML
	private Button loginButton;
	@FXML
	private TextField usernameTextField;
	@FXML
	private TextField serverTextField;
	
/* *****************************************************************************************************************
 	U�wywane funkcje
*/

	/** efekt przycisniecia przycisku login*/
	@FXML
	public void loginButtonOnAction(ActionEvent event){
		String HOST = serverTextField.getText();
		String username = usernameTextField.getText();
		try {
			FXMLLoader loader = new FXMLLoader();
			CustomTalkSceneController controller = new CustomTalkSceneController(HOST, username);
			loader.setController(controller);
			loader.setLocation(getClass().getResource("../views/customTalkScene.fxml"));
			Parent root = loader.load();
			Scene talkScene = new Scene(root);
			Stage window = (Stage) loginButton.getScene().getWindow();
			window.setScene(talkScene);
			window.show();
		} catch (IOException e) {
				e.printStackTrace();
		}
	}
}
package pl.jarekkozmic.controllers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class CustomTalkSceneController implements Initializable {
/* *****************************************************************************************************************
 	U�wywane Dane
*/	
	private Socket clientSocket;
	private DataInputStream inputStream;
	private BufferedReader reader;
	private PrintStream printStream;
	private final int PORT = 6000;
	private String HOST;
	private String username;
	private String receivedMessage;
	private String sentMessage;
	private boolean isWorking;
	private String fileName = "Rozmowa-";
	private File file;
	private String currentDateString;
	private String chatTime;
	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE-dd-MMMM-YYYY");
	private SimpleDateFormat chatTimeFormat = new SimpleDateFormat("yyyy-mm-dd  HH:mm:ss");
	private Date date;
	private FileWriter fileWriter;
	private BufferedWriter writer;

	@FXML
	Button sendButton;
	@FXML
	TextArea readingTextArea;
	@FXML
	TextField writingTextField;
	@FXML
	Button disconnectButton;
	
/* *****************************************************************************************************************
 	U�wywane funkcje
*/	
	/** konstruktor sceny do rozmowy */
	public CustomTalkSceneController(String host, String username) throws IOException{
		HOST = host;
		this.username = username;
		clientSocket = new Socket(HOST, PORT);
		printStream = new PrintStream(clientSocket.getOutputStream());
		inputStream = new DataInputStream(clientSocket.getInputStream());
		reader = new BufferedReader(new InputStreamReader(inputStream));
		date = new Date();
		currentDateString = simpleDateFormat.format(date);
		fileName+=username+"-"+HOST+"-"+currentDateString;
		file = new File(fileName+".txt");
		if(!file.exists()){
			file.createNewFile();
		}
		fileWriter = new FileWriter(file, true);
		writer = new BufferedWriter(fileWriter);
		if(file != null && clientSocket != null && printStream != null && inputStream != null && reader != null)
			isWorking = true;
		else
			isWorking = false;
	}

	/** inicjalizacja sceny, wys�anie imienia do serwera */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		readingTextArea.setEditable(false);
		readingThread.start();
		printStream.println(username);
		printStream.flush();
		writingTextField.setOnKeyPressed(event -> {
			if(event.getCode()==KeyCode.ENTER)
				sendButtonOnAction();
		});
	}
	
	
	/** w�tek s�uchaj�cy serwera, wy�wiertla tylko wtedy gdy na instreamie nie ma nulla*/
	private Thread readingThread = new Thread(()->{
		while(isWorking){
			try {
				if((receivedMessage = reader.readLine()) != null){
					readingTextArea.appendText(receivedMessage+"\n");
					writer.write(receivedMessage+"\n");
				}
				if(receivedMessage.equals("@CLIENT-EXIST")){
					isWorking = false;
					sendButton.setDisable(true);
					disconnectButton.setDisable(true);
				}
			} catch (IOException | NullPointerException e) {
				e.printStackTrace();
				break;
			}
			if(receivedMessage.equals("@SERVER-STOPPED")){
				printStream.println("@OK");
				printStream.flush();
				isWorking = false;
				sendButton.setDisable(true);
				disconnectButton.setDisable(true);
			}
		}
	});



	/** efekt przyci�ni�cia przycisku send  */
	@FXML
	public void sendButtonOnAction(){
		sentMessage = writingTextField.getText();
		Date newDate = new Date();
		chatTime = chatTimeFormat.format(newDate);
		readingTextArea.appendText("<"+username+"> ("+chatTime+"): "+sentMessage+"\n");
		if(printStream != null){
			printStream.println(" ("+chatTime+"): "+sentMessage);
			printStream.flush();
			try {
				writer.write("<"+username+"> ("+chatTime+"): "+sentMessage+"\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		writingTextField.clear();
	}
	/** efekt przycisku disconnect  */
	@FXML
	public void disconnectButtonOnAction() {
		isWorking = false;
		sendButton.setDisable(true);
		disconnectButton.setDisable(true);
		try {
			printStream.println("@QUIT-SERVER");
			printStream.flush();
			reader.close();
			printStream.close();
			inputStream.close();
			writer.close();
			clientSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}

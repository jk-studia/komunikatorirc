package pl.jarekkozmic;
	

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import pl.jarekkozmic.controllers.CustomLoginSceneController;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) throws IOException {
		try{
		FXMLLoader root = new FXMLLoader();
		root.setLocation(getClass().getResource("views/customLoginScene.fxml"));
		AnchorPane layout = root.load();
		Scene scene = new Scene(layout);
		primaryStage.setScene(scene);
		primaryStage.setTitle("IRC Communicator");
		primaryStage.show();
		primaryStage.setResizable(false);
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Blad w tworzeniu okna -Error in class Main");
			System.exit(1);
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}

}

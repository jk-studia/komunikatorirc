package pl.jarekkozmic;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {

        // ===== Utworzenie Gui Serwera
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("views/server.fxml"));
            Parent root = loader.load();
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.setTitle("Communicator Server");
            primaryStage.show();
            primaryStage.setResizable(false);
        }catch(Exception e){
            System.out.println("Nie zaladowano sceny -ERROR in class Main");
            e.printStackTrace();
            System.exit(0);
        }

    }

    public static void main(String[] args) {
        launch(args);
    }
}

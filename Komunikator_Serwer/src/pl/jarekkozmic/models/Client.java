package pl.jarekkozmic.models;

import javafx.application.Platform;
import javafx.scene.control.TextArea;

import java.io.*;
import java.net.Socket;
import java.util.List;

public class Client extends Thread {

    /* *****************************************************************************************************************
         Użwywane Dane
    */
    private String clientName = null;
    private DataInputStream inputStream = null;
    private PrintStream printStream = null;
    private Socket clientSocket;
    private List<Client> clientList;
    private BufferedReader reader = null;
    private boolean clientStoppedWorking = false;
    private TextArea textArea;

/* *****************************************************************************************************************
 	Użwywane funkcje
*/

    /** konstruktor obsługi klienta: potrzebuje gniazdo klienta, listę klientów oraz pole do pokazywania komunikatów*/
    public Client(Socket clientSocket, List<Client> clientList,  TextArea textArea) {
        this.clientList = clientList;
        this.clientSocket = clientSocket;
        this.textArea = textArea;
    }

    /** ustawienie danych obsługi klienta: strumieni wejścia i wyjścia, nazwy klienta */
    public void setClient() throws IOException{
        inputStream = new DataInputStream(clientSocket.getInputStream());
        printStream = new PrintStream(clientSocket.getOutputStream());
        reader = new BufferedReader(new InputStreamReader(inputStream));
    }

    public void setClientStoppedWorking(boolean clientStoppedWorking){
        this.clientStoppedWorking = clientStoppedWorking;
    }

    /** ustawienie imienia oraz przesłanie informacji o nowym kliencie*/
    public synchronized void newClient(String name){
        clientName = name;
        for(Client client: clientList){
            if(client != null && client != this){
                client.printStream.println("<" + clientName + " entered the chatroom>");
                client.printStream.flush();
            }
        }
        printStream.println("<Welcome " + clientName + " to the chatroom>");
        printStream.flush();
    }

    /** przesylanie wiadomosci */
    public synchronized void sendMessage(String line) throws IOException{
        for(Client client: clientList){
            if(client != null && client.clientName != null && client != this){
                client.printStream.println(clientName + ": " + line);
                client.printStream.flush();
            }
        }
    }

    /** przesłanie wiadomości o opuszczeniu przez klienta chatroomu */
    public synchronized void leaveChatRoom() throws IOException{
        // ===== przesłanie wiadomości do klientów o opuszczeniu konwersacji
        for(Client client: clientList){
            if(client != null && client.clientName != null && client != this){
                client.printStream.println("<" + clientName + " left chatroom>");
                client.printStream.flush();
            }
        }
    }

/* *****************************************************************************************************************
 	Obsługa Klienta
*/

    @Override
    public void run() {
        clientStoppedWorking = false;

        try {
            // ===== ustawienie danych
            setClient();

            boolean correctName = true;
            String name = reader.readLine();
            for(Client client: clientList){
                if(client != null && name.equals(client.clientName)){
                    correctName = false;
                }
            }
            if(!correctName){
                printStream.println("@CLIENT-EXIST");
                printStream.flush();
            }

            // ===== powiadomienie użytkowników o nowym kliencie
            if(correctName){
                newClient(name);
                Platform.runLater(()->textArea.appendText("<" + clientName + " ENTERED THE CHATROOM>\n"));
            }
            // ===== słuchanie klienta
            while (correctName){
                String line = reader.readLine();
                if(line != null){
                    if (line.startsWith("@QUIT-SERVER") || clientStoppedWorking) {
                        break;
                    }
                    // ===== przesyłanie wiadomości do wszystkich klientów
                    sendMessage(line);
                }
            }
            if(correctName){
                leaveChatRoom();
                Platform.runLater(()->textArea.appendText("<" + clientName + " LEFT THE CHATROOM>\n"));
                if(!clientStoppedWorking){
                    printStream.println("Bye " + name);
                    printStream.flush();
                }
            }
            inputStream.close();
            printStream.close();
            clientSocket.close();
        } catch (IOException e) {
            System.out.println("IO client -ERROR in class Client");

        }
        finally{
            // ===== zwolnienie pamięci w liście
            clientList.remove(this);
            Platform.runLater(()->textArea.appendText("Ilośc klientów: " + clientList.size() + "\n"));
        }
    }

    public PrintStream getPrintStream() {
        return printStream;
    }
}
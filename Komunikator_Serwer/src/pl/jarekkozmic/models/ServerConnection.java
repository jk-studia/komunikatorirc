package pl.jarekkozmic.models;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Platform;
import javafx.scene.control.TextArea;

public class ServerConnection implements Runnable {

/* *****************************************************************************************************************
 	Używane Dane
*/
    private ServerSocket server = null;
    private Socket clientSocket = null;
    private final int PORT = 6000;
    private final List<Client> clientList = new ArrayList<>();
    private TextArea textArea;
    private boolean isWorking;


/* *****************************************************************************************************************
 	Metody
*/


    /** konstruktor serwera: potrzebne jedynie pole tekstowe do wyświetlania komunikatów w GUI */
    public ServerConnection(TextArea textArea) {
        this.textArea = textArea;
    }

    /** Założenie serwera na podanym porcie, ustawienie flagi: isWorking = true */
    public void createServer() throws IOException, NullPointerException{
        server = new ServerSocket(PORT);
        Platform.runLater(()->textArea.appendText("Uruchomiono Serwer ===== Port: " + PORT + "\n"));
        isWorking = true;
    }


    /** Obsługa klientów, czeka na nowe połączenie.
     Gdy się połączy, dodaje klienta do listy a następnie uruchamia jego osobistą obsługe. */
    public void addNewClient() throws IOException{
        Platform.runLater(()->textArea.appendText("ilosc klientow: " + clientList.size() + "\n"));
        clientSocket = server.accept();
        if(isWorking){
            Client client = new Client(clientSocket, clientList, textArea);
            clientList.add(client);
            clientList.get(clientList.size()-1).start();
        }
    }

    /** Zamknięcie serwera */
    public void close(){
        Platform.runLater(()->textArea.appendText("Zamykanie-serwera\n"));
        isWorking = false;
        try {
            Socket closingSocket = new Socket("localhost", PORT);
            synchronized (clientList) {
                for(Client client: clientList){
                    client.setClientStoppedWorking(true);
                    client.getPrintStream().println("@SERVER-STOPPED");
                }
            }
            closingSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


/* *****************************************************************************************************************
 	Działanie serwera
*/

    @Override
    public void run() {

        // ===== założenie serwera
        try {
            createServer();
        } catch (IOException e) {
            Platform.runLater(()->textArea.appendText("Problem ze strumieniami servera -Error in class ServerConnection\n"));
            isWorking = false;
        } catch(NullPointerException e){
            Platform.runLater(()->textArea.appendText("Nie zalozono serwera -Error in class ServerConnection\n"));
            isWorking = false;
        }

        // ===== obsługa klientów
        while (isWorking) {
            try {
                addNewClient();
            } catch (IOException e) {
                Platform.runLater(()->textArea.appendText("Problem z dodaniem klienta -Error in class ServerConnection"));
                System.out.println(e);
            }
        }

        try {


            for(int i = 0; i < clientList.size(); i++)
                try {
                    clientList.get(i).join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            server.close();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally{
            clientList.clear();
            Platform.runLater(()->textArea.appendText("Pomyslnie zamknieto serwer\n"));
        }
    }
}

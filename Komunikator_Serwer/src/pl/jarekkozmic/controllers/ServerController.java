package pl.jarekkozmic.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import pl.jarekkozmic.models.ServerConnection;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ServerController implements Initializable {

    /* *****************************************************************************************************************
         Użwywane Dane
    */
    @FXML
    private Button startButton;
    @FXML
    private TextArea textArea;

    private ExecutorService service;
    private ServerConnection server = null;

    // ===== początkowa inicjalizacja danych
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        textArea.setEditable(false);
    }

    /* *****************************************************************************************************************
         Metody
    */
    // ===== funkcja obsługująca efekt przyciśnięcia przycisku start, założenie nowego serwera
    @FXML
    public void startOnAction(){
        try{
            service = Executors.newSingleThreadExecutor();
            server = new ServerConnection(textArea);
            service.submit(server);
        }catch(NullPointerException e){
            System.out.println("nie udalo sie zalozyc serwera");
        }
        startButton.setDisable(true);
    }

    // ===== funkcja obsługująca efekt przyciśnięcia przycisku stop, zamknięcie servera
    @FXML
    public void stopOnAction(){
        if(server != null)
            try{
                server.close();
                server = null;
                startButton.setDisable(false);
                service.shutdown();
            }catch(NullPointerException e){
                e.printStackTrace();
            }
    }
}
